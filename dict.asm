%define rbyte 8
global find_word
extern string_equals

section .text

find_word:
    cmp rsi, 0
    je .error
    add rsi, rbyte
    call string_equals
    test rax, rax
    jnz .findAddress
    sub rsi, rbyte
    mov rsi, [rsi]
    jmp find_word

.error:
    xor rax, rax
    ret

.findAddress:
    sub rsi, rbyte
    mov rax, rsi
    ret
