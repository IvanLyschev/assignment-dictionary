%include "words.inc"

global _start
extern print_string
extern print_err_string
extern print_newline
extern string_length
extern exit
extern find_word
extern read_word

%define BUFF_SIZE 256

section .rodata
welcome: db "Welcome, write key: ", 10, 0
found: db "value is: ", 0
not_found: db "key not found", 10, 0
out_of_buffer: db "buffer is overflow", 10, 0
null_line: db 10
section .text
_start:
    mov rdi, welcome
    call print_string

    sub rsp, BUFF_SIZE
    mov rsi, BUFF_SIZE
    mov rdi, rsp
    call read_word
    cmp rdx, BUFF_SIZE
    jg .read_out

    mov rsi, nextElement
    mov rdi, rax
    call find_word
    test rax, rax
    jz .error

    push rax
    mov rdi, found
    call print_string
    pop rax

    mov rdi, rax
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    mov rsi, 1
    call print_string
    mov rdi, null_line
    call print_string
    add rsp, BUFF_SIZE
call exit

.read_out:
    mov rdi, out_of_buffer
    call print_err_string
    call exit

.error:
    mov rdi, not_found
    call print_err_string
    call exit
ret
